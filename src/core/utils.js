module.exports.noop = () => { };

module.exports.generateID = () => Math.floor(Math.random() * 10000000 + 1);

